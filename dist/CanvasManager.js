"use strict";
var CanvasManager = /** @class */ (function () {
    function CanvasManager() {
        this.balls = [];
        this.radius = 20;
        this.canvas = document.getElementById('bouncingCanvas');
        this.ctx = this.canvas.getContext('2d');
        this.initRadiusProgress();
        this.initCanvas();
    }
    CanvasManager.prototype.initCanvas = function () {
        var _this = this;
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        if (this.ctx) {
            this.canvas.addEventListener('click', function (evt) {
                _this.balls.push(new BallAnimator(_this.ctx, evt.offsetX, evt.offsetY, _this.radius));
            });
            requestAnimationFrame(this.tick.bind(this));
        }
    };
    CanvasManager.prototype.tick = function () {
        var _a;
        (_a = this.ctx) === null || _a === void 0 ? void 0 : _a.clearRect(0, 0, this.canvas.width, this.canvas.height);
        for (var _i = 0, _b = this.balls; _i < _b.length; _i++) {
            var ball = _b[_i];
            ball.updateBall();
            ball.drawBall();
        }
        requestAnimationFrame(this.tick.bind(this));
    };
    CanvasManager.prototype.initRadiusProgress = function () {
        var _this = this;
        var progressBar = document.getElementById('radiusProgressbar');
        if (progressBar) {
            this.setRadiusValue(progressBar.value);
            progressBar.addEventListener('input', function (event) {
                var radiusValue = event.target.value;
                _this.setRadiusValue(radiusValue);
                console.log(_this.radius);
            });
        }
    };
    CanvasManager.prototype.setRadiusValue = function (value) {
        this.radius = parseInt(value);
    };
    return CanvasManager;
}());
