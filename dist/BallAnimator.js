"use strict";
var BallAnimator = /** @class */ (function () {
    function BallAnimator(ctx, offsetX, offsetY, radius) {
        this.ctx = ctx;
        this.ball = {
            x: offsetX - radius,
            y: offsetY,
            radius: radius,
            speed: 0,
            gravity: BallAnimator.GRAVITY,
            bounceFactor: BallAnimator.BOUNCE_FACTOR,
        };
        this.endColor = this.generateRandomColor();
    }
    BallAnimator.prototype.drawBall = function () {
        if (!this.ctx) {
            return;
        }
        var gradient = this.getBallGradient();
        this.ctx.beginPath();
        this.ctx.arc(this.ball.x, this.ball.y, this.ball.radius, 0, Math.PI * 2);
        this.ctx.fillStyle = gradient;
        this.ctx.fill();
        this.ctx.closePath();
    };
    BallAnimator.prototype.getBallGradient = function () {
        var vibrationSpeed = BallAnimator.VIBRATION_SPEED;
        var vibrationOffset = Math.sin(this.ball.y * vibrationSpeed) * 5;
        var gradient = this.ctx.createRadialGradient(this.ball.x, this.ball.y + vibrationOffset, 0, this.ball.x, this.ball.y + vibrationOffset, this.ball.radius);
        gradient.addColorStop(0, 'white');
        gradient.addColorStop(1, this.endColor);
        return gradient;
    };
    BallAnimator.prototype.generateRandomColor = function () {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    };
    BallAnimator.prototype.updateBall = function () {
        if (this.shouldBallStop()) {
            this.stopBall();
        }
        this.ball.y += this.ball.speed;
        this.ball.speed += this.ball.gravity;
        if (this.ball.y + this.ball.radius > this.ctx.canvas.height) {
            this.ball.y = this.ctx.canvas.height - this.ball.radius;
            this.ball.speed = -this.ball.speed * this.ball.bounceFactor;
        }
    };
    BallAnimator.prototype.shouldBallStop = function () {
        var whenShouldStop = this.ctx.canvas.height - this.ball.radius / 2;
        return this.ball.y > whenShouldStop - this.ball.radius && (this.ball.speed > 0 && this.ball.speed < 1);
    };
    BallAnimator.prototype.stopBall = function () {
        var _this = this;
        setTimeout(function () {
            _this.ball.speed = BallAnimator.BALL_INITIAL_SPEED;
            _this.ball.y = _this.ctx.canvas.height - _this.ball.radius;
        }, 1000);
    };
    BallAnimator.BALL_INITIAL_SPEED = 0;
    BallAnimator.GRAVITY = 0.5;
    BallAnimator.BOUNCE_FACTOR = 0.7;
    BallAnimator.VIBRATION_SPEED = 0.1;
    return BallAnimator;
}());
