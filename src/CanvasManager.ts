class CanvasManager {
    private canvas: HTMLCanvasElement;
    private ctx: CanvasRenderingContext2D | null;
    private balls: BallAnimator[] = [];
    private radius: number = 20;

    constructor() {
        this.canvas = document.getElementById('bouncingCanvas') as HTMLCanvasElement;
        this.ctx = this.canvas.getContext('2d');

        this.initRadiusProgress()
        this.initCanvas()
    }

    private initCanvas() {
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;

        if (this.ctx) {
            this.canvas.addEventListener('click', (evt) => {
                this.balls.push(new BallAnimator(this.ctx!, evt.offsetX, evt.offsetY, this.radius));
            });

            requestAnimationFrame(this.tick.bind(this));
        }
    }

    private tick() {
        this.ctx?.clearRect(0, 0, this.canvas.width, this.canvas.height);
        for (const ball of this.balls) {
            ball.updateBall();
            ball.drawBall();
        }
        requestAnimationFrame(this.tick.bind(this));
    }

    private initRadiusProgress() {
        const progressBar = document.getElementById('radiusProgressbar') as HTMLInputElement | null;
        if (progressBar) {
            this.setRadiusValue(progressBar.value)

            progressBar.addEventListener('input', (event) => {
                const radiusValue = (event.target as HTMLInputElement).value;
                this.setRadiusValue(radiusValue)
                console.log(this.radius)
            });
        }
    }

    private setRadiusValue(value: string) {
        this.radius = parseInt(value);
    }
}