class BallAnimator {
    private static readonly BALL_INITIAL_SPEED = 0;
    private static readonly GRAVITY = 0.5;
    private static readonly BOUNCE_FACTOR = 0.7;
    private static readonly VIBRATION_SPEED = 0.1;

    endColor: string;

    private ball: {
        x: number;
        y: number;
        radius: number;
        speed: number;
        gravity: number;
        bounceFactor: number;
    };

    constructor(private ctx: CanvasRenderingContext2D, offsetX: number, offsetY: number, radius: number) {
        this.ball = {
            x: offsetX - radius,
            y: offsetY,
            radius: radius,
            speed: 0,
            gravity: BallAnimator.GRAVITY,
            bounceFactor: BallAnimator.BOUNCE_FACTOR,
        };
        this.endColor = this.generateRandomColor()
    }

    public drawBall() {
        if (!this.ctx) {
            return;
        }
        const gradient = this.getBallGradient()

        this.ctx.beginPath();
        this.ctx.arc(this.ball.x, this.ball.y, this.ball.radius, 0, Math.PI * 2);
        this.ctx.fillStyle = gradient;
        this.ctx.fill();
        this.ctx.closePath();
    }

    private getBallGradient() {
        const vibrationSpeed = BallAnimator.VIBRATION_SPEED;
        const vibrationOffset = Math.sin(this.ball.y * vibrationSpeed) * 5;

        const gradient = this.ctx.createRadialGradient(
            this.ball.x, this.ball.y + vibrationOffset, 0,
            this.ball.x, this.ball.y + vibrationOffset, this.ball.radius
        );

        gradient.addColorStop(0, 'white');
        gradient.addColorStop(1, this.endColor);
        return gradient;
    }

    private generateRandomColor(): string {
        const letters = '0123456789ABCDEF';
        let color = '#';

        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }

        return color;
    }

    public updateBall() {
        if (this.shouldBallStop()) {
            this.stopBall();
        }
        this.ball.y += this.ball.speed;
        this.ball.speed += this.ball.gravity;

        if (this.ball.y + this.ball.radius > this.ctx.canvas.height) {
            this.ball.y = this.ctx.canvas.height - this.ball.radius;
            this.ball.speed = -this.ball.speed * this.ball.bounceFactor;
        }
    }

    private shouldBallStop() {
        const whenShouldStop = this.ctx.canvas.height - this.ball.radius / 2;
        return this.ball.y > whenShouldStop - this.ball.radius && (this.ball.speed > 0 && this.ball.speed < 1);
    }

    private stopBall() {
        setTimeout(() => {
            this.ball.speed = BallAnimator.BALL_INITIAL_SPEED;
            this.ball.y = this.ctx.canvas.height - this.ball.radius;
        }, 1000)
    }
}